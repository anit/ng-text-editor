var express = require('express')
var app = express()
var bodyParser =  require('body-parser')
var people = require('./people')

app.use(express.static(__dirname + '/public'))
app.use(bodyParser.json())

app.get('/api/person/:id', function (req, res) {
  res.send(people.get(req.params.id));
});

app.put('/api/person/:id', function (req, res) {
  var result = people.save(req.params.id, req.body);
  res.send(result, result.statusCode || 200);
});


var port = process.env.PORT || 8080
app.listen(port)
