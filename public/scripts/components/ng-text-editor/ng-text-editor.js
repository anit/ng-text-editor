angular.module('ng-text-editor', [])


/**
 * Directive for ng-text-editor
 */

.directive('ngTextEditor', function ($timeout) {

  var template = [
    '<div>',
    '  <span class="ng-text-read-only"',
    '        ng-show="readOnly"',
    '        ng-focus="toggle()"',
    '        ng-click="toggle()"',
    '        ng-class="{ \'ng-text-placeholder\': !ngModel }">',
    '    {{ ngModel || placeholder }}',
    '  </span>',
    '  <span class="input-group ng-text-edit" ng-hide=readOnly>',
    '    <input type="text"',
    '          class="form-control"',
    '          ng-keyup="saveHandler($event)"',
    '          value="{{ ngModel }}"',
    '          ng-blur="toggle()"',
    '          autofocus>',

    '    <span class="input-group-btn">',
    '      <button type="button"',
    '              class="btn btn-save btn-primary"',
    '              ng-mousedown="save()">',
    '        &#10004;',
    '      </button>',
    '      <button type="button"',
    '              class="btn btn-cancel btn-danger"',
    '              ng-mousedown="cancel()">',
    '        &#10006;',
    '      </button>',
    '    </span>',
    '  </span>',
    '</div>'
  ].join(' ');



  return {
    restrict: 'E',
    template: template,
    scope: {
      ngModel: '=',
      placeholder: '@',
      ngChange: '&'
    },

    link: function (scope, element, attrs) {
      var readOnlyEl = element.find('.ng-text-read-only')[0];
      var inputEl = element.find('input')[0];

      scope.readOnly = true;

      // Toggle state
      scope.toggle = function () {
        scope.readOnly = !scope.readOnly;

        // In write mode focus on the text box
        if (!scope.readOnly) {
          $timeout(function () {
            inputEl.focus();

            // Move the position of cursor in the end of text
            inputEl.value = inputEl.value;
          }, 100);
        } else {
          inputEl.value = scope.ngModel || '';
        }
      };

      // On pressing enter save model and on esc cancel changes
      scope.saveHandler = function (event) {
        if (event.keyCode === 13) {
          scope.save();
        } else if (event.keyCode === 27) {
          scope.cancel();
        }
      };

      scope.save = function () {
        scope.ngModel = inputEl.value;
        inputEl.blur();
        scope.ngChange();
      };

      scope.cancel = function () {
        inputEl.value = scope.ngModel;
        inputEl.blur();
      };

    }
  };
});
