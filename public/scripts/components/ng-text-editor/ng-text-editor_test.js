'use strict';

describe('ng-text-editor', function () {
  var templateCache;
  var $document;
  var compile;
  var scope;
  var timeout;
  var directiveEl;

  var setup = function () {
    var template = templateCache.get('public/scripts/components/ng-text-editor/ng-text-editor_test.html');
    var element = angular.element(template);
    var body = angular.element(document.body)
    body.append(element);
    compile(element)(scope);

    directiveEl = {
      labelEl: angular.element(document.querySelector('.ng-text-read-only')),
      textEditEl: angular.element(document.querySelector('.ng-text-edit')),
      buttons: angular.element(document.querySelectorAll('button')),
      scope: angular.element(document.querySelector('.ng-text-edit')).scope()
    };
  };

  beforeEach(module('public/scripts/components/ng-text-editor/ng-text-editor_test.html'));
  beforeEach(module('ng-text-editor'));
  beforeEach(inject(function ($rootScope, $templateCache, _$document_, $compile, $httpBackend, $timeout) {
    scope = $rootScope.$new();
    templateCache = $templateCache;
    $document = _$document_;
    compile = $compile;
    timeout = $timeout;

    scope.model = 'Some good value';
    scope.placeholder = 'Enter value';

    setup();
  }));



  describe('ng-text-editor test', function () {

    it('should show input box with two buttons on click', function () {
      directiveEl.labelEl.triggerHandler('click');
      expect(directiveEl.labelEl.hasClass('ng-hide')).toBe(true);
      expect(directiveEl.textEditEl.hasClass('ng-hide')).toBe(false);
    });


    it('should save on clicking save button', function () {
      directiveEl.labelEl.triggerHandler('click');
      var input = angular.element(document.querySelector('input'));
      var saveBtn = angular.element(document.querySelector('.btn-save'));
      input.val('Hello world');
      saveBtn.triggerHandler('mousedown');

      expect(directiveEl.scope.ngModel).toEqual('Hello world');
    });

    it('should cancel changes on clicking cancel button', function () {
      scope.model = 'OriginalValue';
      scope.$digest();

      directiveEl.labelEl.triggerHandler('click');
      var input = angular.element(document.querySelector('input'));
      var cancelBtn = angular.element(document.querySelector('.btn-cancel'));
      input.val('Hello world');

      cancelBtn.triggerHandler('mousedown');

      expect(directiveEl.scope.ngModel).toEqual('OriginalValue');
    });


    it('should fire change event on change', function () {
      var eventFired = false;
      scope.changed = function (argument) {
        eventFired = true;
      };

      directiveEl.labelEl.triggerHandler('click');
      var input = angular.element(document.querySelector('input'));
      var saveBtn = angular.element(document.querySelector('.btn-save'))

      input.value = "new value";
      saveBtn.triggerHandler('mousedown');

      expect(eventFired).toBe(true);
    });

    it('should display placeholder when model is null', function () {
      scope.model = null;
      scope.placeholder = 'Anything';

      scope.$digest();
      expect(directiveEl.labelEl.html().indexOf('Anything') > -1).toBe(true);
    });
  });

  afterEach(function () {
    angular.element(document.querySelector('body')).empty();
  });
});
