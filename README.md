##AngularJS assignment for eVision

An AngularJS directive that turns the input element into read-only element when the page is loaded with a button to switch it into edit mode. Once in edit mode the directive should display an input control with a save and cancel button. Clicking cancel should undo all changes and turn the element into read only mode. Clicking save should persist the changes to the server using the same endpoint the person was loaded from and, when successful turn the element into read only mode. If saving is not successful, like when saving an empty name, the element should remain in edit mode and display the error message returned from the server. Make sure to include appropriate unit tests for the directive you create.


##Installation

```
npm install
npm start
```

Or for windows

```
start.bat
```
