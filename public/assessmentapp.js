(function (angular) {
    var app = angular.module("assessmentApp", [
      'ng-text-editor'
    ]);
    app.controller('PersonController', function ($scope, $http, $templateCache) {
      $http.get('/api/person/42').success(function (result) {
          $scope.person = result;
          console.log($scope.person);
      });

      $scope.save = function () {
        $scope.alert = {
          message: 'Saving'
        };

        $http.put('/api/person/42', $scope.person).success(function (result) {
          $scope.alert = {
            message: 'Person name changed successfully to ' + $scope.person.fullName + ' on ',
            timeStamp: new Date()
          }
        });
      };

    });
}(window.angular));
