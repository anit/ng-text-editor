## ng-text-editor

 UI Component for editable text.

## Usage

```html
<ng-text-editor ng-model="model" ng-change="change()" placeholder="Enter val">
</ng-text-editor>
```

## API

- `ngModel`: (required) Model for the input
- `ngChange`: Expression to be excecuted when model changes
- `placeholder`: Text to display when model changes
